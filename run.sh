#!/bin/bash
pushd $(dirname $0)

if [[ ! -d .pyenv ]]; then
    virtualenv-3 .pyenv || exit $?
    source .pyenv/bin/activate
    pip install -r requirements.txt
fi

source .pyenv/bin/activate
IMAGE_SIZE=224
ARCHITECTURE="mobilenet_0.50_${IMAGE_SIZE}"
exec python deep_learning_object_detection.py -m MobileNetSSD_deploy.caffemodel -p MobileNetSSD_deploy.prototxt.txt -i $1